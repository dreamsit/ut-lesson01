﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lesson01.SampleProject.Models;
using NUnit.Framework;

namespace Lesson01.SampleProject.UnitTests
{
    [TestFixture]
    public class PointsCalculatorTests
    {
        PointsCalculator _calculator;
        Point _first;
        Point _second;
        Point _secondNull;
        Point _expectedAdd;
        Point _expectedSub;

        [SetUp]
        public void Init()
        {
            _calculator = new PointsCalculator();
            _first = new Point() {X = 2, Y = -1};
            _second = new Point() { X = 5, Y = 18 };
            _secondNull = null;
            _expectedAdd = new Point() {X = 7, Y = 17};
            _expectedSub = new Point() {X = -3, Y = -19};
        }

        [Test]
        [Category("Positive tests")]
        public void AddPoints_ReceiveTwoPoints_ReturnsNotNullResult()
        {
            //arrange
            //act
            var result = _calculator.Add(_first, _second);
            //assert
            Assert.IsNotNull(result, "Result is null");
        }

        [Test]
        [Category("Positive tests")]
        public void AddPoints_ReceiveTwoPoints_ReturnsRightPoint()
        {
            //arrange
            //act
            var result = _calculator.Add(_first, _second);

            //assert
            Assert.IsNotNull(result,"Result is null");
            Assert.AreEqual(_expectedAdd.X, result.X, "X coord is incorrect");
            Assert.AreEqual(_expectedAdd.Y, result.Y, "Y coord is incorrect");
        }

        [Test]
        [Category("Negative tests")]
        [ExpectedException(typeof(ArgumentException))]
        public void AddPoints_ReceivingOnePointAsNull_ThrowsArgumentException()
        {
            //arrange
            //act
            var result = _calculator.Add(_first, _secondNull);
            //assert
        }

        [Test]
        [Category("Positive tests")]
        public void SubPoints_ReceiveTwoPoints_ReturnsRightPoint()
        {
            //arrange
            //act
            var result = _calculator.Sub(_first, _second);

            //assert
            Assert.IsNotNull(result, "Result is null");
            Assert.AreEqual(_expectedSub.X, result.X, "X coord is incorrect");
            Assert.AreEqual(_expectedSub.Y, result.Y, "Y coord is incorrect");
        }

        [Test]
        [Category("Negative tests")]
        [ExpectedException(typeof(ArgumentException))]
        public void SubPoints_ReceivingOnePointAsNull_ThrowsArgumentException()
        {
            //arrange
            //act
            var result = _calculator.Sub(_first, _secondNull);
            //assert
        }
    }
}
