﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lesson01.SampleProject.Models;

namespace Lesson01.SampleProject
{
    public class PointsCalculator
    {
        public Point Add(Point first, Point second)
        {
            _checkInputPoints(first, second);

            return new Point() {X = _calcPoint(first.X,second.X), Y = _calcPoint(first.Y,second.Y)};
        }

        public Point Sub(Point first, Point second)
        {
            _checkInputPoints(first, second);
            Func<int, int, int> calc = (f, s) => f - s;

            return new Point() { X = calc(first.X,second.X), Y = calc(first.Y,second.Y) };
        }

        private void _checkInputPoints(Point first, Point second)
        {
            if (first == null)
                throw new ArgumentException("First point is null");

            if (second == null)
                throw new ArgumentException("Second point is null");
        }

        private int _calcPoint(int first, int second, bool add=true)
        {
            if (add)
                return first + second;
            return first - second;
        }
    }
}
