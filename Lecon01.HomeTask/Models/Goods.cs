﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecon01.HomeTask.Models
{
    public class Goods
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
