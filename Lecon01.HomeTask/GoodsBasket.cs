﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lecon01.HomeTask.Models;

namespace Lecon01.HomeTask
{
    public class GoodsBasket
    {
        private Dictionary<Goods, uint> _goods = new Dictionary<Goods, uint>();

        public ReadOnlyDictionary<Goods, uint> Goods
        {
            get
            {
                return new ReadOnlyDictionary<Goods, uint>(_goods);
            }
        }  

        public void AddGoods(Goods goods, uint count)
        {
            if (!_goods.ContainsKey(goods))
            {
                _goods.Add(goods,count);
            }
            else
            {
                _goods[goods] += count;
            }
        }

        public void RemoveGoods(Goods goods, uint count)
        {
            if (!_goods.ContainsKey(goods))
            {
                throw new Exception("Goods not finded");
            }
            else if (_goods[goods] < count)
            {
                throw new Exception("Can\'t remove so many goods");
            }
            else
            {
                _goods[goods] -= count;
            }
        }

        public decimal CalculateSumm()
        {
            return _goods.Any() ? _goods.Sum(x => x.Key.Price*x.Value) : 0;
        }
    }
}
